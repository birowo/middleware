package main

import (
	"context"
	"net"
	"net/http"
	"net/http/httptest"

	"gitlab.com/birowo/middleware"

	"github.com/valyala/fasthttp"
	"github.com/valyala/fasthttp/fasthttputil"
)

func nethttp() {
	println("net/http :")
	//contoh middleware 1, 2 & 3 :
	mw1 := func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			println("mw1; sebelum")
			next(w, r)
			println("mw1; sesudah")
		}
	}
	mw2 := func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			println("mw2; sebelum")
			next(w, r)
			println("mw2; sesudah")
		}
	}
	mw3 := func(next http.HandlerFunc) http.HandlerFunc {
		return func(w http.ResponseWriter, r *http.Request) {
			println("mw3; sebelum")
			//next(w, r)
			//kita tahu di contoh ini mw3 adalah
			//widdleware terakhir, jadi next-nya nil
			println("mw3; sesudah")
		}
	}
	//http.ListenAndServe(
	//	":8080", middleware.Chain(mw1, mw2, mw3),
	//)
	middleware.Chain(mw1, mw2, mw3)(
		nil, httptest.NewRequest(http.MethodGet, "/", nil),
	)
}
func httpfast() {
	println("fasthttp :")
	//contoh middleware 1, 2 & 3 :
	mw1 := func(next fasthttp.RequestHandler) fasthttp.RequestHandler {
		return func(ctx *fasthttp.RequestCtx) {
			println("mw1; sebelum")
			next(ctx)
			println("mw1; sesudah")
		}
	}
	mw2 := func(next fasthttp.RequestHandler) fasthttp.RequestHandler {
		return func(ctx *fasthttp.RequestCtx) {
			println("mw2; sebelum")
			next(ctx)
			println("mw2; sesudah")
		}
	}
	mw3 := func(next fasthttp.RequestHandler) fasthttp.RequestHandler {
		return func(ctx *fasthttp.RequestCtx) {
			println("mw3; sebelum")
			//next(ctx)
			//kita tahu di contoh ini mw3 adalah
			//widdleware terakhir, jadi next-nya nil
			println("mw3; sesudah")
		}
	}
	//fasthttp.ListenAndServe(":8181", middleware.Chain(mw1, mw2, mw3))
	s := &fasthttp.Server{
		Handler: middleware.Chain(mw1, mw2, mw3),
	}
	ln := fasthttputil.NewInmemoryListener()
	defer ln.Close()
	go s.Serve(ln)
	(&http.Client{Transport: &http.Transport{
		DialContext: func(_ context.Context, _, _ string) (net.Conn, error) {
			return ln.Dial()
		},
	}}).Get("http://-/")
}
func main() {
	nethttp()
	httpfast()
}
