package middleware

type mw[T any] func(T) T

func Chain[T any](mws ...mw[T]) (hndlr T) {
	for i := len(mws) - 1; i > -1; i-- {
		hndlr = mws[i](hndlr)
	}
	return
}
